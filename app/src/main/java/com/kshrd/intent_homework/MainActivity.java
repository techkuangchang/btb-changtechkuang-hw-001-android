package com.kshrd.intent_homework;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    ImageView imageView, imageView1;

    TextView textViewMain;

    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView1 = (ImageView) findViewById(R.id.imageView2);

        /********** Intent Image 1 ***************/
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("Image", R.drawable.cat);
                intent.putExtra("Message", getResources().getString(R.string.placeholderTxt));
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                Log.i("Image1","is Started");
            }
        });

        /********** Intent Image 2 ***************/
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("Image", R.drawable.cat_2);
                intent.putExtra("Message", getResources().getString(R.string.placeholderTxt1));
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
                Log.i("Image2","is Started");
            }
        });
    }

    /********** Get Text ***************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SECOND_ACTIVITY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                String returnString = data.getStringExtra("txtPopup");

                textViewMain = (TextView) findViewById(R.id.txtPopupMain);
//                textViewMain.setText(returnString);
                Toast.makeText(this,returnString,Toast.LENGTH_LONG).show();

            }
        }
    }
}