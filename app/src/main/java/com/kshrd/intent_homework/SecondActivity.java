package com.kshrd.intent_homework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView = (TextView) findViewById(R.id.txtView);
        imageView = (ImageView) findViewById(R.id.imageViewSecond);

        int imageID = getIntent().getIntExtra("Image",0);
        Bundle bundle = getIntent().getExtras();
        String messages = bundle.getString("Message");

        textView.setText(messages);
        imageView.setImageResource(imageID);
    }

    public void btnSend(View view) {

        editText = (EditText) findViewById(R.id.editText);
        String txt = this.editText.getText().toString();
        Intent intent = getIntent();
        intent.putExtra("txtPopup",txt);
        setResult(RESULT_OK,intent);
        Log.i(txt,"Work");
        finish();

    }
}